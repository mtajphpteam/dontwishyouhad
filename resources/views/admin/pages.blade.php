@extends('layouts.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- About Me Box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="col-md-10">
                                <h3 class="box-title" style="margin-top: 5px;"> List Of Pages</h3>
                            </div>

                            <div class="col-md-2 ">
                                <a href="{{route('admin.addPage')}}" class="btn btn-block btn-primary btn-sm">Add New Page</a>
                            </div>

                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>

            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Title</th>
                            <th>Status </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($pages as $page)
                            <tr>
                                <td>{{$page->id}}</td>
                                <td>{{$page->name}}</td>
                                <td>{{$page->slug}}</td>
                                <td>{{$page->title}}</td>
                                <td>
                                    @if($page->status == 1) Active @else De-Active @endif</td>
                                <td class="text-center" width="140px">
                                    <a href="{{route('admin.viewPage',['id' => $page->id])}}" class="viewicon"> <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
                                    <a href="{{route('admin.editPage',['id' => $page->id])}}" class="editicon"> <i class="fa fa-pencil-square fa-2x margin-r-15 margin-l-15" aria-hidden="true"></i></a>
                                    <a href="#" class="deleteicon deletepage"  data-val="{{ $page->id }}"> <i class="fa fa-trash fa-2x " aria-hidden="true"></i></a>
                                </td>
                            </tr>

                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Name</th>
                            <th>Slug</th>
                            <th>Title</th>
                            <th>Status </th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section><!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('js')
<script>
    $(function () {
        $('#example1').DataTable({

        })

    })
</script>
@endsection
