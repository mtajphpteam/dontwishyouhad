@extends('layouts.main')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                    <!-- About Me Box -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <div class="col-md-10">
                                <h3 class="box-title" style="margin-top: 5px;"> List Of Medias</h3>
                            </div>

                            <div class="col-md-2 ">
                                <a href="{{ route('admin.addMedia') }}" class="btn btn-block btn-primary btn-sm">Add Media</a>
                            </div>

                        </div>
                    </div>
                    <!-- /.box -->
                </div>
            </div>

            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="example1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Link</th>
                            <th>Date</th>
                            <th>Status </th>
                            <th>Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach($media as $m)
                            <tr>
                                <td>{{$m->id}}</td>
                                <td>{{$m->title}}</td>
                                <td>{{$m->link}}</td>
                                <td>{{$m->media_date}}</td>
                                <td>
                                    @if($m->status == 1) Active @else De-Active @endif</td>
                                <td class="text-center" width="140px">
                                    <a href="{{route('admin.viewMedia',['id' => $m->id])}}" class="viewicon"> <i class="fa fa-eye fa-2x" aria-hidden="true"></i></a>
                                    <a href="{{route('admin.editMedia',['id' => $m->id])}}" class="editicon"> <i class="fa fa-pencil-square fa-2x margin-r-15 margin-l-15" aria-hidden="true"></i></a>
                                    <a href="#" class="deleteicon deleteMedia"  data-val="{{ $m->id }}"> <i class="fa fa-trash fa-2x " aria-hidden="true"></i></a>
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                        <tfoot>
                        <tr>
                            <th>Id</th>
                            <th>Title</th>
                            <th>Link</th>
                            <th>Date</th>
                            <th>Status </th>
                            <th>Action</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </section><!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection

@section('js')
    <script>
        $(function () {
            $('#example1').DataTable({

            })

        })
    </script>
@endsection
