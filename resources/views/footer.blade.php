
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="fbox">
                        <div class="ftitle">Follow Us</div>
                        <ul class="list-inline socialist">
                            <li> <a href="https://www.facebook.com/" target="_blank"><i class="fa fa-facebook-f"></i></a></li>
                            <li> <a href="https://twitter.com/?lang=en" target="_blank"><i class="fa fa-twitter"></i></a></li>
                            <li> <a href="https://in.pinterest.com/" target="_blank"><i class="fa fa-pinterest-p"></i></a></li>
                            <li> <a href="https://www.instagram.com/?hl=en" target="_blank"><i class="fa fa-instagram"></i></a></li>
                            <li> <a href="https://plus.google.com/discover" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                        </ul>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </footer>

    <section class="footerlnks_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footerlinks">
                        <ul class="list-inline flinks">
                            <li><a href="javascript:void(0);">In The Media</a></li>
                            <li><a href="javascript:void(0);">Testimonials</a></li>
                            <li><a href="javascript:void(0);">FAQ</a></li>
                            <li><a href="javascript:void(0);">Get Inspired</a></li>
                            <li><a href="javascript:void(0);">Message Types</a></li>
                            <li><a href="javascript:void(0);">Public Messages</a></li>
                            <li><a href="javascript:void(0);">Contact Us</a></li>
                            <li><a href="javascript:void(0);">Partner With Us</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="copywrap">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 col-sm-6 col-xs-6 tctrl">
                    <div class="copydiv">
                        <p>&copy; Copyright 2018, Dontwishyouhad.com</p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6 tctrl">
                    <ul class="list-inline cplinks">
                        <li><a href="javascript:void(0);">Privacy Policy</a></li>
                        <li><a href="javascript:void(0);">Terms of Use</a></li>
                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>

    <div class="back_to_top_wrap">
        <a href="javascript:void(0);" class="back_to_top"><i class="fa fa-arrow-up"></i></a>
    </div>
    <!-- Login Modal Code Starts Here -->
    <div class="modal fade custom_modal" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="modal_logodiv">
                                <img src="{{asset('assets/frontend/images/logo.png')}}" alt="">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <form id="signinForm" >
                        {{csrf_field()}}
                    <div class="row">
                        <div class="col-md-offset-2 col-md-8 formdiv">
                            <div class="form-group" id="signin-email-field">
                                <input class="form-control tbox" name="email" id="signinemail" placeholder="Enter your email" type="email">
                                <div class="text-danger signin-email-error" style="display: none;"></div>
                            </div>
                            <div class="form-group" id="signin-password-field">
                                <input class="form-control tbox" name="password" id="signinpassword" placeholder="Password" type="password">
                                <div class="text-danger signin-password-error" style="display: none;"></div>
                            </div>
                            <div class="buttondiv">
                                <button type="button" onclick="signinSubmit()" class="btn btn-default sbtn">Login</button>
                            </div>
                            <div class="otherlinksdiv">
                                <p>Don't have an account? <a href="javascript:void(0);" data-toggle="modal" data-target="#signupModal" data-dismiss="modal">Sign Up</a></p>
                                <p><a href="javascript:void(0);" data-toggle="modal" data-target="#forgotPassModal" data-dismiss="modal">Forgot your password?</a></p>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <!-- Login Modal Code Ends Here -->

    <!-- Signup Modal Code Starts Here -->
    <div class="modal fade custom_modal" id="signupModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="modal_logodiv">
                                <img src="{{asset('assets/frontend/images/logo.png')}}" alt="">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <form id="signupForm" >
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8 formdiv">
                                <div class="form-group" id="email-field">
                                    <input class="form-control tbox" id="email" name="email" placeholder="Enter your email" type="email">
                                    <div class="text-danger email-error" style="display: none;"></div>
                                </div>
                                <div class="form-group" id="password-field">
                                    <input class="form-control tbox" id="password" name="password" placeholder="Password" type="password">
                                    <div class="text-danger  password-error" style="display: none;"></div>
                                </div>
                                <div class="form-group" id="confirmpass-field">
                                    <input class="form-control tbox" id="confirmpass" name="password_confirmation" placeholder="Confirm Password" type="password">
                                    <div class="text-danger confirmpass-error" style="display: none;"></div>
                                </div>

                                <div class="otherlinksdiv">
                                    <p>I have read and agreed to the <a href="javascript:void(0)" target="_blank">Terms of Service</a></p>
                                </div>
                                <div class="buttondiv">
                                    <button type="button" onclick="signupSubmit()" class="btn btn-default sbtn">Get your Free Account</button>
                                    <p>(30-day free trial)</p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                    <div id="model_loader" style="display: none;">
                        <div class="loadme-foldingCube">
                            <div class="loadme-foldingCube-child1 loadme-foldingCube-child"></div>
                            <div class="loadme-foldingCube-child2 loadme-foldingCube-child"></div>
                            <div class="loadme-foldingCube-child4 loadme-foldingCube-child"></div>
                            <div class="loadme-foldingCube-child3 loadme-foldingCube-child"></div>
                        </div>
                        <div class="loadme-mask"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div id="fullpageloader" style="display: none;">
        <div class="loadme-foldingCube">
            <div class="loadme-foldingCube-child1 loadme-foldingCube-child"></div>
            <div class="loadme-foldingCube-child2 loadme-foldingCube-child"></div>
            <div class="loadme-foldingCube-child4 loadme-foldingCube-child"></div>
            <div class="loadme-foldingCube-child3 loadme-foldingCube-child"></div>
        </div>
        <div class="loadme-mask"></div>
    </div>

    <!-- forgotpassword Modal Code Starts Here -->
    <div class="modal fade custom_modal" id="forgotPassModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="modal_logodiv">
                                <img src="{{asset('assets/frontend/images/logo.png')}}" alt="">
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <form id="forgotpassForm" >
                        {{csrf_field()}}
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8">
                                <p>No worries, we will send you an email to replace your password when pressing on the "Replace Password" button below.</p>
                                <br>
                                <br>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-offset-2 col-md-8 formdiv">
                                <div class="form-group" id="email-field">
                                    <input class="form-control tbox" id="forgot-email" name="email" placeholder="Enter your email" type="email">
                                    <div class="text-danger email-error" style="display: none;"></div>
                                </div>
                                <div class="buttondiv">
                                    <button type="button" onclick="forgotpassSubmit()" class="btn btn-default sbtn">Replace Password</button>

                                </div>
                                <div class="otherlinksdiv">
                                    <p>Don't have an account? <a href="javascript:void(0);" data-toggle="modal" data-target="#signupModal" data-dismiss="modal">Sign Up</a></p>
                                    <p><a href="javascript:void(0);" data-toggle="modal" data-target="#loginModal" data-dismiss="modal">Login</a></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </form>
                    <div id="model_loader" style="display: none;">
                        <div class="loadme-foldingCube">
                            <div class="loadme-foldingCube-child1 loadme-foldingCube-child"></div>
                            <div class="loadme-foldingCube-child2 loadme-foldingCube-child"></div>
                            <div class="loadme-foldingCube-child4 loadme-foldingCube-child"></div>
                            <div class="loadme-foldingCube-child3 loadme-foldingCube-child"></div>
                        </div>
                        <div class="loadme-mask"></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <!-- forgotpassword Modal Code Ends Here -->

    <script src="{{asset('assets/frontend/js/jquery-3.1.0.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/main.js')}}"></script>

<script>
    //Header Fix Code
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 95) {
            $('header').addClass('fix-header');
            $('.back_to_top_wrap').show();

        }
        else {
            $('header').removeClass('fix-header');
            $('.back_to_top_wrap').hide();
        }
    });

</script>


<script>
    //Back To Top Code
    $('.back_to_top_wrap').on('click', '.back_to_top', function (e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, 800);
    });
</script>

    <script>
        function signinSubmit()
        {
            var postData = $("#signinForm").serialize();
            //console.log(postData);
            $('#model_loader').css('display','block');
            $.ajax({
                url: "{{ route('signin') }}",
                type:'POST',
                dataType : 'json',
                data: postData,
                success: function(data) {
                    console.log(data);
                    if(data.status == 1){

                      window.location= "{{route('userDashboard')}}"

                    }
                    else if(data.status == 2)
                    {
                        $('#model_loader').hide();
                        swal({
                            title: 'Warning!',
                            text:data.message ,
                            type: 'warning',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            //$('#verifyOTPModal').modal('show');
                        });
                    }else if(data.status == 0)
                    {
                        $('#model_loader').hide();
                        swal({
                            title: 'Fail!',
                            text:data.message ,
                            type: 'error',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            //$('#verifyOTPModal').modal('show');
                        });
                    }
                    else{
                        $('#loader').hide();
                        if(data.message.email)
                        {
                            $('#signinemail').addClass('has-error');
                            $('.signin-email-error').show();
                            $('.signin-email-error').html(data.message.email);
                        }else
                        {
                            $('#signinemail').removeClass('has-error');
                            $('.signin-email-error').hide();
                        }

                        if(data.message.password)
                        {
                            $('#signinpassword').addClass('has-error');
                            $('.signin-password-error').show();
                            $('.signin-password-error').html(data.message.password);

                        }else
                        {
                            $('#signinpassword').removeClass('has-error');
                            $('.signin-password-error').hide();
                        }

                    }
                }
            });
        }

        function signupSubmit()
        {

        var postData = $("#signupForm").serialize();
        //console.log(postData);
            $('#model_loader').css('display','block');

        $.ajax({
            url: "{{ route('signup') }}",
            type:'POST',
            dataType : 'json',
            data: postData,
            success: function(data) {
            console.log(data);
                if(data.status == 1){

                  $('#signupModal').modal('hide');
                    $('#loader').hide();
                    swal({
                        title: 'Success!',
                        text: 'A confirmation email has been sent to your email address. Please press the link in the email to activate your account. In case you do not receive your activation email, please make sure to check your spam or junk folders.',
                        type: 'success',
                        confirmButtonText: 'Ok'
                    }).then(function(){
                        //$('#verifyOTPModal').modal('show');
                    });

                } else{
                    $('#loader').hide();
                    if(data.message.email)
                    {
                        $('#email').addClass('has-error');
                        $('.email-error').show();
                        $('.email-error').html(data.message.email);
                    }else
                    {
                        $('#email').removeClass('has-error');
                        $('.email-error').hide();
                    }

                    if(data.message.password)
                    {
                        $('#password').addClass('has-error');
                        $('.password-error').show();
                        $('.password-error').html(data.message.password);

                    }else
                    {
                        $('#password').removeClass('has-error');
                        $('.password-error').hide();
                    }

                    if(data.message.password_confirmation)
                    {
                        $('#confirmpass').addClass('has-error');
                        $('.confirmpass-error').show();
                        $('.confirmpass-error').html(data.message.password_confirmation);

                    }else
                    {
                        $('#confirmpass').removeClass('has-error');
                        $('.confirmpass-error').hide();
                    }

                }
            }
        });

        }

        function forgotpassSubmit()
        {

            var postData = $("#forgotpassForm").serialize();
            //console.log(postData);
            $('#model_loader').css('display','block');

            $.ajax({
                url: "{{ route('forgotPassword') }}",
                type:'POST',
                dataType : 'json',
                data: postData,
                success: function(data) {
                    console.log(data);
                    if(data.status == 1){

                        $('#signupModal').modal('hide');
                        $('#loader').hide();
                        swal({
                            title: 'Success!',
                            text: data.message,
                            type: 'success',
                            confirmButtonText: 'Ok'
                        }).then(function(){
                            //$('#verifyOTPModal').modal('show');
                        });

                    } else{
                        $('#loader').hide();
                        if(data.message.email)
                        {
                            $('#email').addClass('has-error');
                            $('.email-error').show();
                            $('.email-error').html(data.message.email);
                        }else
                        {
                            $('#email').removeClass('has-error');
                            $('.email-error').hide();
                        }

                    }
                }
            });
        }
    </script>

    <script>
        @if(session()->has('success_message'))
            swal("Here's the title!", "...and here's the text!");
        @endif
    </script>
    <script>
        @if(session()->has('error_message'))
        swal("Here's the title!", "...and here's the text!");
        @endif
    </script>

</body>
</html>