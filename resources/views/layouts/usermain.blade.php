<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Don't wish you had</title>


    <link href="{{asset('assets/frontend/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/font-awesome.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->




</head>
<body>
<section class="home_header">
    <div class="container">
        <div class="row">

            <div class="col-md-1 col-sm-2 col-xs-12">
                <div class="homelogo">
                    <a href="index.html"><img src="{{asset('assets/frontend/images/logo.png')}}" class="img-responsive"  alt="Logo"></a>
                </div>
                <div class="navbar-header resp-div">
                    <button type="button" class="navbar-toggle collapsed resp-btn" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>


                <div class="clearfix"></div>

            </div>

            <div class="col-md-11 col-sm-10 col-xs-12">

                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

                    <div class="row homemenurow">
                        <div class="col-md-6 col-sm-6 col-xs-5 tctrl">
                            <ul class="list-inline home_menu_links">
                                <li><a href="#">My Safes</a></li>
                                <li><a href="#">My Assets</a></li>
                            </ul>
                        </div>
                        <div class="col-md-6 col-sm-6 col-xs-7 tctrl">
                            <div class="profilemenu_box">

                                <div class="profile_pic">
                                    <img src="{{asset('assets/frontend/images/ppic.png')}}" alt="" class="img-responsive">
                                </div>


                                <div class="pmenu_box">


                                    <ul class="nav navbar-nav prof_menu_links">
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                                                Profile Name <span class="caret"></span>
                                            </a>
                                            <ul class="dropdown-menu psmenu">
                                                <li><a href="#">Profile Edit</a></li>
                                                <li><a href="#">Action1</a></li>
                                                <li><a href="#">Action2</a></li>
                                                <li><a href="#">Log Out</a></li>
                                            </ul>
                                        </li>
                                    </ul>



                                    <div class="profile_prgrs">
                                        <span class="pvalue">40%</span>
                                        <div class="progress">
                                            <div class="progress-bar progress-bar-striped active" role="progressbar"
                                                 aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width:40%">

                                            </div>
                                        </div>
                                    </div>
                                    <div class="shortlinks">
                                        <ul class="list-inline con_links">
                                            <li><a href="#"><i class="fa fa-question"></i></a></li>
                                            <li><a href="#"><i class="fa fa-lightbulb-o"></i></a></li>
                                            <li><a href="#"><i class="fa fa-search"></i></a></li>
                                        </ul>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

        </div>
    </div>

</section>


@yield('content')

<footer>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="fbox">
                    <div class="ftitle">Follow Us</div>
                    <ul class="list-inline socialist">
                        <li> <a href="javascript:void(0);"><i class="fa fa-facebook-f"></i></a></li>
                        <li> <a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                        <li> <a href="javascript:void(0);"><i class="fa fa-pinterest-p"></i></a></li>
                        <li> <a href="javascript:void(0);"><i class="fa fa-instagram"></i></a></li>
                        <li> <a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                    </ul>
                    <div class="clearfix"></div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</footer>

<section class="footerlnks_wrap">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="footerlinks">
                    <ul class="list-inline flinks">
                        <li><a href="javascript:void(0);">In The Media</a></li>
                        <li><a href="javascript:void(0);">Testimonials</a></li>
                        <li><a href="javascript:void(0);">FAQ</a></li>
                        <li><a href="javascript:void(0);">Get Inspired</a></li>
                        <li><a href="javascript:void(0);">Message Types</a></li>
                        <li><a href="javascript:void(0);">Blog</a></li>
                        <li><a href="javascript:void(0);">Public Messages</a></li>
                        <li><a href="javascript:void(0);">Contact Us</a></li>
                        <li><a href="javascript:void(0);">Partner With Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="copywrap">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6 tctrl">
                <div class="copydiv">
                    <p>&copy; Copyright 2018, Dontwishyouhad.com</p>
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6 tctrl">
                <ul class="list-inline cplinks">
                    <li><a href="javascript:void(0);">Privacy Policy</a></li>
                    <li><a href="javascript:void(0);">Terms of Use</a></li>
                </ul>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</section>

<div class="back_to_top_wrap">
    <a href="javascript:void(0);" class="back_to_top"><i class="fa fa-arrow-up"></i></a>
</div>


<script src="{{asset('assets/frontend/js/jquery-3.1.0.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>
<script src="{{asset('assets/frontend/js/main.js')}}"></script>

<script>
    //Header Fix Code
    $(window).scroll(function(){
        if ($(window).scrollTop() >= 95) {
            $('header').addClass('fix-header');
            $('.home_header').addClass('fix-header');
            $('.back_to_top_wrap').show();
        }
        else {
            $('header').removeClass('fix-header');
            $('.home_header').removeClass('fix-header');
            $('.back_to_top_wrap').hide();
        }
    });

    //Back To Top Code
    $('.back_to_top_wrap').on('click', '.back_to_top', function (e) {
        e.preventDefault();
        $('html, body').animate({scrollTop: 0}, 800);
    });
</script>

@yield('js');

<script>

    //File Upload Code Starts Here
    /*	$(document).ready(function()
        {
           $("#file_source").on('change',function(){

               var fname = $("#file_source").val();
                if (fname.match(/fakepath/)) {
                    // update the file-path text using case-insensitive regex
                    fname = fname.replace(/C:\\fakepath\\/i, '');
                }
                if(fname!=""){
                    $("#upload-file-info").text(fname);
                }

               });
        });*/
</script>
</body>
</html>