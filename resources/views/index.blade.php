@extends('layouts.frontmain')

@section('content')
    <section id="homeCarousel" class="carousel slide carousel-fade" data-ride="carousel" data-interval="false">



        <ol class="carousel-indicators">
            <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#homeCarousel" data-slide-to="1"></li>
            <li data-target="#homeCarousel" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
            <div class="item active">
                <img src="{{asset('assets/frontend/images/slider1.jpg')}}" class="simg">

                <div class="carousel-caption hidden-xs">
                    <div class="heading">
                        <h1>The Digital Time Capsule</h1>
                    </div>
                    <p>Emotional life insurance</p>
                    <ul class="list-unstyled clist">
                        <li>Pellentesque vel dolor euismod, finibus ante id, rhoncus velit.</li>
                        <li>Phasellus nec dolor vulputate, finibus odio non, egestas augue.</li>
                        <li>Duis vestibulum erat euismod magna volutpat, nec dictum nunc eleifend.</li>
                        <li>In placerat tortor sed lectus facilisis, a condimentum magna pretium.</li>
                    </ul>
                    <div class="morelinksbbox">
                        <a href="javascript:void(0);"><i class="fa fa-play-circle-o"></i>Learn more</a>
                        <a href="javascript:void(0);">Get started for free</a>
                    </div>
                </div>

            </div>
            <div class="item">
                <img src="{{asset('assets/frontend/images/slider1.jpg')}}" class="simg">

                <div class="carousel-caption hidden-xs">
                    <div class="heading">
                        <h1>The Digital Time Capsule</h1>
                    </div>
                    <p>Emotional life insurance</p>
                    <ul class="list-unstyled clist">
                        <li>Pellentesque vel dolor euismod, finibus ante id, rhoncus velit.</li>
                        <li>Phasellus nec dolor vulputate, finibus odio non, egestas augue.</li>
                        <li>Duis vestibulum erat euismod magna volutpat, nec dictum nunc eleifend.</li>
                        <li>In placerat tortor sed lectus facilisis, a condimentum magna pretium.</li>
                    </ul>
                    <div class="morelinksbbox">
                        <a href="javascript:void(0);"><i class="fa fa-play-circle-o"></i>Learn more</a>
                        <a href="javascript:void(0);">Get started for free</a>
                    </div>
                </div>

            </div>
            <div class="item">
                <img src="{{asset('assets/frontend/images/slider1.jpg')}}" class="simg">

                <div class="carousel-caption hidden-xs">
                    <div class="heading">
                        <h1>The Digital Time Capsule</h1>
                    </div>
                    <p>Emotional life insurance</p>
                    <ul class="list-unstyled clist">
                        <li>Pellentesque vel dolor euismod, finibus ante id, rhoncus velit.</li>
                        <li>Phasellus nec dolor vulputate, finibus odio non, egestas augue.</li>
                        <li>Duis vestibulum erat euismod magna volutpat, nec dictum nunc eleifend.</li>
                        <li>In placerat tortor sed lectus facilisis, a condimentum magna pretium.</li>
                    </ul>
                    <div class="morelinksbbox">
                        <a href="javascript:void(0);"><i class="fa fa-play-circle-o"></i>Learn more</a>
                        <a href="javascript:void(0);">Get started for free</a>
                    </div>
                </div>

            </div>
        </div>
        <a class="left carousel-control hidden" href="#homeCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control hidden" href="#homeCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>

        <div class="slantdiv1 hidden-xs"></div>


    </section>

    <section class="messages_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-4 left_side">
                    <div class="msgbox">
                        <div class="msg_img"><img src="{{asset('assets/frontend/images/m1.jpg')}}" alt="" class="img-responsive"></div>
                        <div class="msg_desc">
                            <div class="mtitle">Location Messages</div>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam a neque quis quam varius commodo eget tempor erat.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 right_side">
                    <div class="row">
                        <div class="col-md-7 bttop">
                            <div class="msgbox">
                                <div class="msg_img"><img src="{{asset('assets/frontend/images/m2.jpg')}}" alt="" class="img-responsive"></div>
                                <div class="msg_desc">
                                    <div class="mtitle">Date Messages</div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam a neque quis quam varius commodo eget tempor erat.</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-5 btleft">
                            <div class="msgbox">
                                <div class="msg_img"><img src="{{asset('assets/frontend/images/m3.jp')}}g" alt="" class="img-responsive"></div>
                                <div class="msg_desc">
                                    <div class="mtitle">Events Messages</div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam a neque quis quam varius commodo eget tempor erat.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-7 btright">
                            <div class="msgbox">
                                <div class="msg_img"><img src="{{asset('assets/frontend/images/m4.jpg')}}" alt="" class="img-responsive"></div>
                                <div class="msg_desc">
                                    <div class="mtitle">Social Media Messages</div>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nam a neque quis quam varius commodo eget tempor erat.</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>
        <div class="slantdiv1 hidden-xs"></div>
    </section>

    <section class="public_msg_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="introheading">
                        <div class="heading">
                            <h2>Public Messages</h2>
                        </div>
                        <p>Phasellus nec dolor vulputate, finibus odio non, egestas augue.</p>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="pmsgbox">
                                <img src="{{asset('assets/frontend/images/p1.jpg')}}" class="img-responsive">
                                <div class="pmsg_overlay"><a href="javscript:void(0);">Watch Heidi's Story</a></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="pmsgbox">
                                <img src="{{asset('assets/frontend/images/p2.jpg')}}" class="img-responsive">
                                <div class="pmsg_overlay"><a href="javscript:void(0);">Watch Heidi's Story</a></div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>

                    <div class="morestorydiv">
                        <a href="javascript:void(0);" class="morestory">More Customer Stories</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="slantdiv3 hidden-xs"></div>
    </section>

    <section class="home_invite_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="home_invite_box">
                        <div class="heading">
                            <h3>Do you have a loved one who needs to plan?</h3>
                        </div>
                        <p>Invite them to start their plan with Cake.</p>
                        <a href="javascript:void(0);" class="hinvite">Invite</a>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="slantdiv2 hidden-xs"></div>
    </section>

    <section class="testmsg_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="testmsg_box">
                        <p class="per_msg">Donec eget ornare nibh tiam laoreet enim id volutpat gravid uspendisse potent nteger malesuada tristique faucibus ed in libero eget neque aliquam gravida a sit amet dolor.</p>
                        <div class="test_per_cntdtls">
                            <p>Personal Name</p>
                            <p>City Name, State</p>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="slantdiv2 hidden-xs"></div>
    </section>

    <section class="getstarted_wrap">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="heading">
                        <h3>Get started with Cake today, it's free.</h3>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="row get_start_box formdiv">
                <div class="col-md-4 col-sm-4">
                    <div class="form-group">
                        <input type="email" class="form-control tbox" placeholder="Enter your email">
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="form-group">
                        <input type="password" class="form-control tbox" placeholder="Enter password">
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="form-group">
                        <input type="password" class="form-control tbox" placeholder="Confirm Password">
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="invitebtndiv">
                        <button type="submit" class="btn btn-default sbtn">Signup</button>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </section>
@endsection